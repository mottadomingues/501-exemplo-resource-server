package br.com.itau.aplicacao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AplicacaoController {

	@GetMapping("/plr")
	public Map<String, String> consultarPlr(){
		HashMap<String, String> resultado = new HashMap<String, String>();
		resultado.put("plr", "R$ 12.000,00");
		
		return resultado;
	}
	
	@GetMapping("/prad")
	public Map<String, String> consultarPrad(){
		HashMap<String, String> resultado = new HashMap<String, String>();
		resultado.put("prad", "R$ 5.000,00");
		
		return resultado;
	}
}
